FROM python:3.6-slim
ENV PYTHONUNBUFFERED 1
ENV APP_DIR /code
WORKDIR $APP_DIR

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . $APP_DIR
RUN python setup.py install

CMD ./bin/entrypoint.sh
EXPOSE 8000
