from PIL import Image

from indio.db import get_or_create_db


def analyze_image(uuid_, image_path, private):
    db = get_or_create_db()
    with Image.open(image_path) as img:
        width, height = img.size

    image = db.one('find/by_uuid', keys=[uuid_])['value']

    image.update({
        'width': width,
        'height': height,
        'private': private})
    doc = db.save(image)
    return doc
