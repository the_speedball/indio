"""
Algorithm on finding image with closest dimension to a missing one.

Each image when uploaded has it's "weight" calculated, and those weights are
then compared when looking for a closest one.
As image dimension can be represented by a point on x&y graph. Having a point
we can calculate closeness of two points using Pythagorean theorem. In order
to calculate closeness we have to have two points, as weight is calculated
when uploading an image, there is only one point. I decided to compare
distance to point (0, 0), as it can be calculated without having second point (image dimensions).

But weight of 800*600 and 600*800 (images with flipped dimensions) will be the same someone might say. This is correct. I introduced one additional variable into my equation. Distance from point (0, 0) is multiplied by division of weight by height. Such operation will make flipped dimensions stand out from each other and hopefully work as I imagine.

"""

import os
import math
from tempfile import NamedTemporaryFile

from typing import Tuple
from PIL import Image

from indio.db import db

ImagePath = str


def save_image(uuid_: str, image_file: Image, static_folder: str) -> str:
    save_path = os.path.join(static_folder, uuid_)
    image_file.save(save_path)
    return save_path


def calc_weight(width: int, height: int) -> float:
    return math.sqrt(width**2 + height**2) * (width / height)


def weight_view_range(width: int, height: int,
                      threshold: float) -> Tuple[float, float]:
    weight = calc_weight(width, height)
    return weight - threshold, weight + threshold


def find_similar(width: int, height: int, threshold: float = 0.0):
    threshold += width / 10
    if width - threshold <= 0 or height - threshold <= 0:
        raise FileNotFoundError

    min_, max_ = weight_view_range(width, height, threshold)
    images = db.query(
        'find/by_weight', startkey=min_, endkey=max_, as_list=True)

    # no images found
    if not images:
        print('No images, look again, threshold: ', threshold)
        return find_similar(width, height, threshold * 2)

    base_img_weight = calc_weight(width, height)

    weight_diff = [(abs(base_img_weight - i['key']), i['value']['path'])
                   for i in images]

    img = min(weight_diff, key=lambda x: x[0])
    return img


def find_image_or_similar(width: int, height: int) -> Tuple:
    # get image with same dimensions
    img = db.one('find/by_dimension', key=[width, height], include_docs=True)
    if img:
        return img['doc']['path'], False

    img = find_similar(width, height)
    return img[1], True


def resize_image(image_path: ImagePath, width: int, height: int,
                 static_folder: str) -> ImagePath:
    image = Image.open(os.path.join(static_folder, image_path))
    size = width, height
    image.thumbnail(size)

    tf = NamedTemporaryFile(dir=static_folder, delete=False)
    os.chmod(tf.name, 644)

    image.save(tf, image.format)
    return os.path.basename(tf.name)
