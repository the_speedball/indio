from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import BooleanField

allowed_extensions = ['jpg', 'png', 'jpeg']


class ImageForm(FlaskForm):
    image = FileField(
        'Image',
        validators=[
            FileRequired(),
            FileAllowed(allowed_extensions,
                        f'Only {allowed_extensions} allowed.')
        ])
    is_image_private = BooleanField(
        'Private?', description='Show on home page?', default='checked')
