import os

import pycouchdb

NON_PRIVATE_IMAGES_VIEW = {
    '_id': '_design/non_private',
    'views': {
        'paths': {
            'map':
            r'''function(doc) {
                if (!doc.private) {
                    emit(doc.uuid);
                }
            }'''
        }
    }
}

DOCUMENT_BY_UUID = {
    '_id': '_design/find',
    'views': {
        'by_dimension': {
            'map':
            r'''
            function(doc) {emit([doc.width, doc.height], null);}
            '''
        },
        'by_weight': {
            'map':
            r'''
               function(doc) {
  var weight = Math.sqrt(Math.pow(doc.width, 2) + Math.pow(doc.height, 2)) * (doc.width / doc.height);
  emit(weight, {path: doc.path, width: doc.width, height: doc.height});
}
            '''  # noqa
        },
        'by_uuid': {
            'map':
            r'''
            function(doc) {
                emit(doc.uuid, doc);
            }
            '''
        }
    }
}


def get_or_create_db():
    server = pycouchdb.Server(
        os.getenv('DB_HOST', 'http://admin:password@db:5984'))
    db_name = 'img-meta'

    if os.getenv('TESTING'):
        db_name = 'test-img-meta'

    try:
        db = server.create(db_name)
        db.save(NON_PRIVATE_IMAGES_VIEW)
        db.save(DOCUMENT_BY_UUID)
    except pycouchdb.exceptions.Conflict:
        db = server.database(db_name)

    return db


db = get_or_create_db()
