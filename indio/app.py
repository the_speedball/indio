import os
import uuid

from flask import Flask, render_template, redirect, url_for
from rq import Queue

from indio.img import save_image, resize_image, find_image_or_similar
from indio.forms import ImageForm
from indio.worker import conn
from indio.tasks import analyze_image
from indio.db import db

app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY')
q = Queue(connection=conn)


@app.route('/', methods=['GET', 'POST'])
def home():
    """ Displays latest uploaded public images """
    form = ImageForm()
    if form.validate_on_submit():
        uuid_ = str(uuid.uuid4())
        image_path = save_image(uuid_, form.image.data, app.static_folder)
        relative_path = os.path.relpath(image_path, start=app.static_folder)
        db.save({'uuid': uuid_, 'path': relative_path})
        q.enqueue_call(
            func=analyze_image,
            args=(uuid_, image_path, form.is_image_private.data))

        return redirect(url_for('images', image_id=uuid_))

    found_docs = db.query('non_private/paths', include_docs=True)
    return render_template('home.html', form=form, docs=found_docs)


@app.route('/images/<string:image_id>')
def images(image_id):
    image = db.one('find/by_uuid', keys=[image_id])
    if not image:
        return 'Image not found.', 404
    return render_template('image.html', image=image.get('value'))


@app.route('/images/<int:width>/<int:height>')
def random_image(width, height):
    try:
        image_path, needs_resize = find_image_or_similar(width, height)
    except FileNotFoundError:
        return 'Could not find image according to your specs.', 404

    if needs_resize:
        image_path = resize_image(image_path, width, height, app.static_folder)

    return render_template(
        'image_search.html', image_path=image_path, width=width, height=height)
