# Python Code Challenge #

## Option 1: Image Hosting ##

Using you favorite python web framework, create a simple image hosting application.

Requirements:

  * Home page should display latest uploaded ​ public​ images
  * You should be able to upload an image and retrieve a unique share link
  * When uploading an image, there should be an option to make it ​ private​ (so that it doesn’t appear on the homepage)
  * A special endpoint /image/\<width>/\<height>/(for ex. ​ http://example.com/image/800/600/) that returns a ​ public ​ image with the given dimensions.
      * If there isn’t an image with the exact dimensions, the endpoint should return a resized, randomly selected image, that has the closest dimensions,
      * The same url should always return the same image,
      * Valid parameters are: width>=0 and height>=0.
  
Notes:

  * Pillow is a great image processing library: ​ https://pillow.readthedocs.io/en/5.0.0/ 
  * The frontend design style doesn’t matter(it could be plain minimalistic HTML)

# Actual Readme #

## Running ##

First build images by executing command

    `docker-compose build`
    
Then run images by executing

    `docker-compose up -d`
    
Next open in a browser http://localhost


## Testing ##

Tests require DB to be run. 
Please spin up db with following command:

    `docker-compose up -d db`
    
And run tests with

    `pipenv run pytest`
    
or if you use virtualenv with plain

    `pytest`


