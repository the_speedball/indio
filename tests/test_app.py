def test_home(client, server):
    home = client.get('/')
    assert home.status_code == 200


def test_get_existing_image(client, document):
    image = client.get('/images/3cea73f6-b762-45d2-aab5-5b44b503d873')
    assert image.status_code == 200


def test_get_non_existing_image(client):
    image = client.get('/images/welp_nope')
    assert image.status_code == 404


def test_get_random_image(client, document):
    image = client.get('/images/800/600')
    assert image.status_code == 200


def test_random_image_not_found(client):
    image = client.get('/images/2/2')
    assert image.status_code == 404
