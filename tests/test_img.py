import pytest

from indio.img import calc_weight, weight_view_range


def test_calc_weight_800_600():
    assert calc_weight(800, 600) == pytest.approx(1333, 1)


def test_weight_view_range_800_600_threshold_10():
    assert weight_view_range(800, 600, 10) == (pytest.approx(1333, 1),
                                               pytest.approx(1343, 1))
