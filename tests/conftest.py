import os

import pytest
import pycouchdb

os.environ['TESTING'] = 'True'
os.environ['DB_HOST'] = 'http://admin:password@localhost:5984'

from indio.db import db  # noqa
from indio.app import app  # noqa


@pytest.fixture(scope='session')
def server():
    server = pycouchdb.Server(os.getenv('DB_HOST'))
    yield server
    server.delete('test-img-meta')


@pytest.fixture
def client():
    # TODO: app needs to be installed for testing
    app.config['WTF_CSRF_ENABLED'] = False
    app.static_folder = '../tests'
    return app.test_client()


@pytest.fixture
def document():
    db.save({
        'uuid': '3cea73f6-b762-45d2-aab5-5b44b503d873',
        'width': '700',
        'height': '500',
        'path': 'yo.jpg'
    })
