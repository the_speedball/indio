from setuptools import setup

setup(
    name='indio',
    version='0.0.1',
    packages=['indio'],
    install_requires=['flask'],
    tests_require=['pytest'],
    author='Michal Klich',
    author_email='m@michalkli.ch',
    setup_requires=['pytest-runner'],
    zip_safe=True)
